/*jshint unused:false*/

$('.eop').on('click', function(){
  // Can remove this
  alert('END OF PROCESS - GO TO DASHBOARD WALKTHROUGH');
});


if (!$.support.transition){
  $.fn.transition = $.fn.animate;
}

function nextStep(elm) {
        var form = elm.closest('form');
        var $formelements = $(form).find('.form-group');
        $formelements.each(function(index) {
            $(this).delay(120*index).transition({ x: 210, opacity:0 }, 500, 'ease');
            $(this).transition({ opacity:0 });
        }).promise().done( function(){ 
          $(form).transition({opacity:0})
                  .css('display','none');

            var nextform = $(form).next('form');
            $(nextform).css('display','block')
                        .transition({opacity:1});
          
        } );
}

function nextStepcall(elm) {
        var form = elm.closest('form');
        var $formelements = $(form).find('.form-group');
        $formelements.each(function(index) {
            $(this).delay(120*index).transition({ x: 210, opacity:0 }, 500, 'ease');
            $(this).transition({ opacity:0 });
        }).promise().done( function(){ 
          $(form).transition({opacity:0})
                  .css('display','none');

            var nextform = $('form#frmLogin-4');
            $(nextform).css('display','block')
                        .transition({opacity:1});
          
        } );
}



  $( "input" ).focus(function( ) {
    $( this ).parent().addClass('on');
  });
  $( "input" ).focusout(function( ) {
    if( !$(this).val() ) {
      $( this ).parent().removeClass('on');
    }
  });


$.validator.methods.email = function( value, element ) {
  return this.optional( element ) || /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test( value );
};

var validator_step1 = $("#frmLogin-1").validate({
    // onfocusout: function(element) {
    //             this.element(element);
    //     },

        submitHandler: function () {
          $("#frmLogin-1").find('.cta-area').transition({opacity:0});
          var elm = $("#frmLogin-1").find('.next');
          var first = document.getElementById('num_tel'),
              second = document.getElementById('callbackno');
              second.value = first.value;

              first = document.getElementById('signup_name').value;
              $( ".username" ).replaceWith( first );

          nextStep(elm);
      },

     
});


$('#frmLogin-3 .next, #frmLogin-2 .next').on('click', function(){
    var elm = $(this);

    nextStep(elm);
});


$('#frmLogin-2 .next-call').on('click', function(){
    var elm = $(this);
    nextStepcall(elm);
});

var validator_step3 = $("#frmLogin-3").validate({

      submitHandler: function () {
          // $("#frmLogin-3").find('.cta-area').transition({opacity:0});
          // var elm = $("#frmLogin-3").find('.next');
          // nextStep(elm);
      },

    rules: {
        loa_auth: {
          required: true
        },
          bill1: {
          required: true,
          extension: "jpg|jpeg|pdf|png"
        },
          bill2: {
          required: true,
          extension: "jpg|jpeg|pdf|png"
        },
      },
    errorClass: "error-feedback error",
    errorElement: "small",
    errorPlacement: function(error, element) {
      error.insertBefore(element);
    }

     
});

